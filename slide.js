/**
 * Created with JetBrains RubyMine.
 * User: avinchadee
 * Date: 30/09/13
 * Time: 11:16 AM
 * To change this template use File | Settings | File Templates.
 */
$(function() {
    $( "#sl1" ).slider({
        range: false,
        min: 0,
        max: 500,
        value: [ 40 ]
    });
});

$(function() {
    $( "#sl2" ).slider({
        range: false,
        min: 0,
        max: 100,
        value: [ 20 ]
    });
});

$(function() {
    $( "#sl3" ).slider({
        range: false,
        min: 0,
        max: 10,
        value: [ 0 ]
    });
});

$(function() {
    $( "#sl4" ).slider({
        range: false,
        min: 0,
        max: 10,
        value: [ 5 ]
    });
});