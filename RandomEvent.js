
var currentValues = {currentPlannedTotal:0, currentUnPlannedTotal:0, currentGoodProd:0, currentReWork:0, currentScrap:0};
var currentTotals = {totalPlannedEvents:0, totalUnPlannedEvents:0, totalGoodProd:0, totalReWork:0, totalScrap:0};
var prodFreq = {};
var AllSts;

//function generateRandomDataEvent(numberOfEvents, severity, block) {
//
//}

//function generateRandomDataEvent_2(numberOfEvents, stoppage, severity, variation, shifts) {
//
//    //defines the key location of the production events
//    var keyLocation = "Endeavourland";
//
//    $("#locationID").html(keyLocation);
//
//    // finds all blocksthat production events can be put into
//    AllSts =  find_all_shifts_with_same_location(keyLocation, shifts);
//
//    find_production_frequencies((severity)/10);
//
//    if (typeof AllSts[0] === 'undefined') {
//        console.log(AllSts.length);
//        //create instance of endeavour shift
//
//        createBlankShift();
//        AllSts.push(usableShifts);
//
//        console.log(AllSts);
//    }
//    else{
//        console.log(AllSts);
//    }
//    //runAll(globalShifts);
//}

function find_last_block(shift){
    var last_time = basis_time;
    for(var i = 0; i < shift[0].blocks.length ; i++){
        if(last_time > shift[0].blocks[i].end_time){
            last_time = shift[0].blocks[i].end_time;
        }
    }
    return last_time;
}

function find_all_shifts_with_same_location(location, shifts){
    var availableShifts = new Array();
    for(var j = 0; j < shifts.length; j++){
        for(var i = 0; i < shifts[j].blocks.length; i++){
            if(shifts[j].blocks[i].location.name == location && availableShifts){
                console.log(shifts[j].blocks[i].location.name);
                availableShifts.push(shifts[j]);
            }
        }
    }
    return availableShifts;
}

function find_production_frequencies(severity){
    prodFreq["Good"] = 1 - ((Math.random() * 0.2 + 0.7) * severity);
    prodFreq["Rework"] = (Math.random() * (1 - prodFreq["Good"]));
    prodFreq["Waste"] = 1 - prodFreq["Good"] - prodFreq["Rework"];
}

function generate_suitable_random_good_production_event(eventAverage, totalGoodProduction, actualOutput){

}

function getFreq(){
    return prodFreq;
}


function getCurrentVals(){
    return currentValues;
}

function setCurrentVals(key, value){
    currentValues[key] = value;
}

function setCurrentTotals(key, value){
    currentTotals[key] = value;
}

function getCurrentTotals(){
    return currentTotals;
}

function clearShifts(){
    availableShifts = new Array();
}
