/**
 * Created with JetBrains RubyMine.
 * User: avinchadee
 * Date: 29/09/13
 * Time: 2:34 PM
 * To change this template use File | Settings | File Templates.
 */
var totalGood = 0;
var totalWaste = 0;
var totalRework = 0;



function createProductEvent(block, type, frequency){
    var amount = block.throughput.value / frequency;

    var quantity = getQuantity(amount, type);
    var now = moment().format();

    send({
      description: "[" + now + "] Sensor generated event",
      quantity: quantity,
      reporter_id: 8,
      type: type,
      time: now,
      block_id: block.id
    });

}

function getQuantity(value, type){
  var tempQuan = 0;
  if(type == "GoodProductEvent"){
      totalGood+= value;
      tempQuan = totalGood;
  }
  else if(type == "WasteEvent"){
      totalWaste+= value;
      tempQuan = totalWaste;
  }
  else if(type == "ReworkEvent")  {
      totalRework+= value;
      tempQuan = totalRework;
  }
  return {
    value: tempQuan,
    units: "kg"
  };
}

function send(productionEventData) {

  $.ajax({
        type: "POST",
        url: host + 'events',
        data: productionEventData,
        async: false,
        success: function (event) {
          console.error('Sent production event', event);
        }
      }
  );

}