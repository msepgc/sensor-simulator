var intervalGood;
var intervalWaste;
var intervalRework

var totaltime = 60000;

function runAll(block) {
  var totalWaste = 0;
  var totalRework = 0;
  var totalGood = 0;

  console.error('runAll', block);

  find_production_frequencies($("#sl3").val() / 10);

  var freqGood = totaltime / ($("#sl1").val() * getFreq()["Good"]);


  console.error('freqGood', freqGood);

  intervalGood = setInterval(function () {
    if (totalGood < $("#sl1").val() * getFreq()["Good"]) {
      console.log(block);
      createProductEvent(block, "GoodProductEvent", freqGood);
      $("#GPTotalValue").html(totalGood);
    } else if (totalGood > $("#sl1").val() * getFreq()["Good"]) {
      console.error('Stopping intervalGood');
      window.clearInterval(intervalGood);
    }
    totalGood++;
  }, freqGood);

  intervalWaste = setInterval(function () {
    if (totalWaste < $("#sl1").val() * getFreq()["Waste"]) {

      createProductEvent(block, "WasteEvent", totaltime / ($("#sl1").val() * getFreq()["Rework"]));
      $("#WSTotalValue").html(totalWaste);
    } else if (totalWaste > $("#sl1").val() * getFreq()["Waste"]) {
      window.clearInterval(intervalWaste);
      console.error('Stopping intervalWaste');
    }
    totalWaste++;
  }, totaltime / ($("#sl1").val() * getFreq()["Waste"]));

  intervalRework = setInterval(function () {
    if (totalRework < $("#sl1").val() * getFreq()["Rework"]) {
      createProductEvent(block, "ReworkEvent", totaltime / ($("#sl1").val() * getFreq()["Rework"]));
      $("#RWTotalValue").html(totalRework);
    } else if (totalRework > $("#sl1").val() * getFreq()["Rework"]) {
      window.clearInterval(intervalRework);
      console.error('Stopping intervalRework');
    }
    totalRework++;
  }, totaltime / ($("#sl1").val() * getFreq()["Rework"]));
}

function stopAll() {
  window.clearInterval(intervalRework);
  window.clearInterval(intervalWaste);
  window.clearInterval(intervalGood);
}