var globalShift;

var globalProdQuant;
var globalLoc;
var globalBlocks;
var globalShift;

var basis_time;
var host = window.location.protocol + '//' + window.location.host + '/kono-server/';

$(document).ready(function () {
  $("#visibleDataHeader").hide();
  $("#back-btn").hide();
  $("#rem1").hide();
//    $.get(host + "shifts/", function (shift) {
//        globalShift = shift;
//    })
//        .fail(function () {
//            console("failed to get shifts");
//        });
});

$("#rndDataBtn").click(function () {
  $("#rndDataBtn").prop("disabled", true);

  basis_time = moment();
  createDefaultLocation();
  createDefaultProductQuantities();
  createBlankShift();
  createBlankBlock(globalShift, globalShift.start_time, globalShift.end_time);

  generateRandomData();
  runAll(globalBlocks);
  //createBlankData();
});

$("#back-btn").click(function () {
  $("#rndDataBtn").prop("disabled", false);
  $("#visibleDataHeader").hide("slow");
  $("#back-btn").hide("slow");
  $("#rem1").hide("slow");
  pageScrollUp();
  stopAll();
});

function generateRandomData() {
  $("#visibleDataHeader").show("slow");
  $("#back-btn").show();

  //number of events to create
  var numberOfEvents = $("#sl1").val();

  //severity is the duration the line is down for and the product quantity
  //low severity = more micro-stops etc. more good product
  //high severity = long delays on lines, more waste and rework
  var severityOfEvents = $("#sl3").val();

  //variation defines how binded the algorithm is to a set of blocks
  //Alot of variation means the data will latch onto other blocks
  //very little means that it will latch onto very few blocks
  var variation = $("#sl4").val();

  //stoppage likely-hood reflects how often the (collection of) lines will stop
  //ie, of the total Duration events how many stopped the line
  var stoppage = $("#sl2").val();

//  generateRandomDataEvent(numberOfEvents, severityOfEvents, globalblocks);

  pageScrollDown();
}

//function handleData(data) {
//  usableShifts = data;
//  console.error(1);
//}

function createBlankShift() {
  var end_time = basis_time.clone().add('minutes', 1);
  $.ajax({
        type: "POST",
        url: host + 'shifts',
        data: {
          start_time: basis_time.format(),
          end_time: end_time.format()

        },
        async: false,
        success: function (shift) {
          globalShift = shift;
        }
      }
  );
  console.error(2);

}

function createDefaultLocation() {
  $.ajax({
        type: "POST",
        url: host + 'locations',
        data: {
          name: 'Endeavourland'
        },
        async: false,
        success: function (location) {
          //console.log(location);
          globalLoc = location;
        }
      }
  );
}

function createDefaultProductQuantities() {
  var product = {id: 2};
  $.when($.ajax({
            type: "POST",
            url: host + 'product_quantities',
            data: {
              product_id: product.id,
              quantity: {value: 200, units: 'kg'}
            },
            async: false
          }
      )).done(function (PQuant) {
        globalProdQuant = PQuant;
      });
}

function createBlankBlock(shift, startTime, endTime) {
  $.ajax({
        type: "POST",
        url: host + 'blocks',
        data: {
          shift_id: shift.id,
          location_id: globalLoc.id,
          start_time: moment(startTime).format(),
          end_time: moment(endTime).format(),
          throughput: {value: 2000, units: 'kg/sec'},
          product_quantity_id: globalProdQuant.id
        },
        async: false,
        success: function (block) {
          globalBlocks = block;
//          usableShifts[0].push(blocks);
        }
      }
  );
}

function pageScrollDown() {
  $("html, body").animate({
    scrollTop: $("#visibleDataHeader").offset().top - 50
  }, "slow");

  $("#rem1").show("slow");
}

function pageScrollUp() {
  $.get(host + "shifts/", function (shift) {
    globalShift = shift;
  })
      .fail(function () {
        console("failed to get shifts");
      });

  $("html, body").animate({
    scrollTop: $("#titleKono").offset().top - 100
  }, "slow");

  clearShifts();
}